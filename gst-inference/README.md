# gst-inference

- [Source](https://github.com/RidgeRun/gst-inference.git)
- [Documentation](https://developer.ridgerun.com/wiki/index.php?title=GstInference/Introduction)


## Example

- Build then run Docker image

```
$ ./build.sh
$ ./image.sh
```

- Inside the image run this script:

```
$ ./test.sh
```

NN should run on the GPU if supported.