# gst-video-analytics

- [Source](https://github.com/opencv/gst-video-analytics)
- [Documentation](https://opencv.github.io/gst-video-analytics/)

## Example

- We'll use
[official images from Intel](https://hub.docker.com/r/openvino/ubuntu18_data_dev)
so we can just download and run it using:

```
$ ./image.sh
```

- Inside the image then run this script:

```
$ ./test.sh
```
