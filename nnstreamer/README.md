# NNStreamer

- [Source](https://github.com/nnstreamer/nnstreamer)
- [Documentation](https://github.com/nnstreamer/nnstreamer/wiki)

## Example

- Build then run Docker image

```
$ ./build.sh
$ ./image.sh
```

- Inside the image run one of the following script:

```
$ ./test.sh
$ ./test2.sh
```