# gst-ml

The goal of this repo is to gather informations about the various
[GStreamer](https://gstreamer.freedesktop.org) machine learning
frameworks and provide an easy way to test each of them.

For each framework Docker files and scripts are provided to easily setup
the environnement and run a pipeline.
Just check the `README` in each subdirectory:

- [DeepStream](deepstream/README.md)
- [gst-inference](gst-inference/README.md)
- [NNStreamer](nnstreamer/README.md)
- [gst-video-analytics](gst-video-analytics/README.md)

**legal note:** this repo contains various ML models which I'm not sure about the redistribution,
so please keep this Collabora internal only for now.

## Setup

The scripts are using [Podman](https://podman.io) to build images and run containers.

In order to get GPU support you'll have to install
[nvidia-container-toolkit](https://github.com/NVIDIA/nvidia-docker). I did this on
Fedora 32 by [adding their rpm repo](https://github.com/NVIDIA/nvidia-docker#rhel-docker-or-podman)
and installing the `nvidia-container-toolkit`package.
I also had to workaround [some bug](https://github.com/NVIDIA/nvidia-container-runtime/issues/85)
by setting `no-cgroups = true` in `/etc/nvidia-container-runtime/config.toml`.