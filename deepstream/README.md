# DeepStream

- [Project page](https://developer.nvidia.com/deepstream-sdk)

## Example

- We'll be using official Nvidia docker images so we just need to download
and run it

```
$ ./image.sh
```

- Test app are shipped with the image so you can use those directly,
for example:

```
$ cd deepstream_sdk_v4.0.2_x86_64/samples/configs/deepstream-app
$ deepstream-app -c source1_usb_dec_infer_resnet_int8.txt
$ deepstream-app -c source4_1080p_dec_infer-resnet_tracker_sgie_tiled_display_int8.txt
```
